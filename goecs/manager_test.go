package goecs

import (
	"testing"
	"time"
)

func TestCreateEntity(t *testing.T) {
	var ecs = NewECS()

	var entity = ecs.CreateEntity()
	if entity != 0 {
		t.Error("Expected first entity to be 0, was", entity)
	}

	if len(ecs.entities) != 1 {
		t.Error("Expected entities map to contain one entity, has", len(ecs.entities))
	}
}

func TestRegisterComponent(t *testing.T) {
	var ecs = NewECS()

	type TestComponent struct {
		TestValue int
	}

	ecs.RegisterComponent(TestComponent{})
	if ecs.bitIndex != 2 {
		t.Error("Expected bitIndex to increase, was", ecs.bitIndex)
	}

	// TODO catch error (not panic) for exceeding bitfield
}

func TestAddComponent(t *testing.T) {
	var ecs = NewECS()

	type TestComponent struct {
		TestValue int
	}
	ecs.RegisterComponent(TestComponent{})

	var entity = ecs.CreateEntity()
	ecs.AddComponent(entity, TestComponent{TestValue: 5})
	if len(ecs.componentIndex) != 1 {
		t.Error("Expected component index to increase by 1")
	}

	var testComponent, ok = ecs.components["TestComponent"][entity].(TestComponent)
	if !ok {
		t.Error("Casting from Component -> TestComponent failed")
	} else if testComponent.TestValue != 5 {
		t.Error("Expected to receive TestComponent with TestValue = 5, was", testComponent.TestValue)
	}
}

func TestGetComponent(t *testing.T) {
	var ecs = NewECS()

	type TestComponent struct {
		TestValue int
	}
	ecs.RegisterComponent(TestComponent{})

	var entity = ecs.CreateEntity()
	ecs.AddComponent(entity, TestComponent{TestValue: 5})

	var component, err = ecs.GetComponent(entity, TestComponent{})
	if err != nil {
		t.Error("Unexpected error retrieving component:", err)
	}

	var testComponent, ok = (component).(TestComponent)
	if !ok {
		t.Error("Casting from Component -> TestComponent failed")
	} else if testComponent.TestValue != 5 {
		t.Error("Expected to received TestComponent with TestValue = 5, was", testComponent.TestValue)
	}
}

func TestEntitiesWith(t *testing.T) {
	var ecs = NewECS()

	type TestComponent struct {
		TestValue int
	}
	ecs.RegisterComponent(TestComponent{})

	// add some dummy entities
	ecs.CreateEntity()
	ecs.CreateEntity()

	var entity = ecs.CreateEntity()
	ecs.AddComponent(entity, TestComponent{TestValue: 5})

	var entities = ecs.EntitiesWith(TestComponent{})
	if len(entities) != 1 {
		t.Error("Only expected to get 1 entity back, got", len(entities))
	}

	if entities[0] != entity {
		t.Error("Didn't receive back the expected entity. Wanted", entity, "but got", entities[0])
	}

	// now we test searching when other components are registered
	type TestComponent2 struct {
		TestValue2 int
	}
	ecs.RegisterComponent(TestComponent2{})

	var otherEntity = ecs.CreateEntity()
	ecs.AddComponent(otherEntity, TestComponent2{TestValue2: 10})

	// we should get the same results as the first test
	entities = ecs.EntitiesWith(TestComponent{})
	if len(entities) != 1 {
		t.Error("Only expected to get 1 entity back, got", len(entities))
	}

	if entities[0] != entity {
		t.Error("Didn't receive back the expected entity. Wanted", entity, "but got", entities[0])
	}
}

func BenchmarkEntityPlusEntityXComparison(b *testing.B) {
	// These are designed to match the tests run by EntityPlus
	// https://github.com/Yelnats321/EntityPlus

	benchmarks := []struct {
		name          string
		numEntities   int
		numIterations int
		probability   int
	}{
		{"1k   x 100k w/ 33%", 1000, 100000, 3},
		{"10k  x 100k w/ 33%", 10000, 100000, 3},
		{"30k  x 10k  w/ 33%", 30000, 10000, 3},
		{"100k x 10k  w/ 20%", 100000, 10000, 5},
		{"10k  x 100k w/ 0.1%", 10000, 100000, 1000},
		{"100k x 100k w/ 0.1%", 100000, 100000, 1000},
	}

	for _, benchmark := range benchmarks {

		// set up ECS with a single component
		type TestComponent struct {
			TestValue int
		}

		var ecs = NewECS()
		ecs.RegisterComponent(TestComponent{})

		// fill ECS with benchmark.numEntities and add a component based on benchmark.probability
		for i := 0; i < benchmark.numEntities; i++ {
			var entity = ecs.CreateEntity()

			if i%benchmark.probability == 0 {
				ecs.AddComponent(entity, TestComponent{TestValue: 1})
			}
		}

		// search through ECS benchmark.numIterations times
		b.ResetTimer()
		start := time.Now()
		for i := 0; i < benchmark.numIterations; i++ {
			var entities = ecs.EntitiesWith(TestComponent{})

			// adding up a sum as an abitrary use of the test component data
			sum := 0
			for _, entity := range entities {
				comp, _ := ecs.GetComponent(entity, TestComponent{})
				testComp := comp.(TestComponent)
				sum += testComp.TestValue
			}
		}
		b.Log(benchmark.name, time.Since(start))
	}
}
