package goecs

import (
	"fmt"
	"testing"
)

func TestSet(t *testing.T) {
	var bits Bitfield

	// 00000001
	bits = Set(bits, 1)

	if bits != 1 {
		t.Error("Expected 00000001, was", fmt.Sprintf("%08b", bits))
	}

	// 00000101
	bits = Set(bits, 4)
	if bits != 5 {
		t.Error("Expected 00000101, was", fmt.Sprintf("%08b", bits))
	}
}

func TestClear(t *testing.T) {
	// 00001100
	var bits Bitfield = 12

	// 00000100
	bits = Clear(bits, 8)
	if bits != 4 {
		t.Error("Expected 00000100, was", fmt.Sprintf("%08b", bits))
	}

	// 00000000
	bits = Clear(bits, 4)
	if bits != 0 {
		t.Error("Expected 00000000, was", fmt.Sprintf("%08b", bits))
	}
}

func TestHas(t *testing.T) {
	// 00001100
	var bits Bitfield = 12

	if !Has(bits, 8) {
		t.Error("Expected 3rd bit to be set for", fmt.Sprintf("%08b", bits))
	}

	if !Has(bits, 4) {
		t.Error("Expected 2nd bit to be set for", fmt.Sprintf("%08b", bits))
	}
}

func TestHasAll(t *testing.T) {
	// 00001100
	var bits Bitfield = 12

	if !HasAll(bits, 12) {
		t.Error("Expected 2nd and 3rd bits to be set for", fmt.Sprintf("%08b", bits))
	}
}
