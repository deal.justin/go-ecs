// This file contains everything needed to manage the
// bitfields used for tracking what components an entity
// has attached. The bitfield is expected to be managed by
// the ECS type in manager.go

package goecs

// Bitfield defines one flag per bit
type Bitfield uint32 // 32 flags available

// Set a specific flag (0 -> 1)
func Set(field Bitfield, flag Bitfield) Bitfield {
	return field | flag
}

// Clear a specific flag (1 -> 0)
func Clear(field Bitfield, flag Bitfield) Bitfield {
	return field &^ flag
}

// Has a specific flag (don't use compound flags, only one flag)
func Has(field Bitfield, flag Bitfield) bool {
	return field&flag != 0
}

// HasAll of the provided flags (typical ECS search query)
func HasAll(field, flags Bitfield) bool {
	return field&flags == flags
}
