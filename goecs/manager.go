package goecs

import (
	"errors"
	"reflect"
)

// Entity is just an identifier that can be stored anywhere
type Entity uint32

// Component interface behind all user defined components
type Component interface{}

// ECS is the overall manager of entities and components
type ECS struct {
	entityIndex    Entity
	bitIndex       Bitfield
	entities       map[Entity]Bitfield
	entityViews    map[string]map[Entity]Bitfield
	components     map[string]map[Entity]Component
	componentIndex map[string]Bitfield
}

// NewECS will provide a fresh ECS instance
func NewECS() *ECS {
	var ecs = new(ECS)

	ecs.bitIndex = 1
	ecs.entities = map[Entity]Bitfield{}
	ecs.entityViews = map[string]map[Entity]Bitfield{}
	ecs.components = map[string]map[Entity]Component{}
	ecs.componentIndex = map[string]Bitfield{}

	return ecs
}

// CreateEntity will initialize a new entity within ECS and return it's UUID
func (ecs *ECS) CreateEntity() Entity {
	var entity = ecs.entityIndex
	ecs.entityIndex++

	var b Bitfield
	ecs.entities[entity] = b

	return entity
}

// RegisterComponent for use with entities
func (ecs *ECS) RegisterComponent(component Component) {
	// get the component type
	var componentType = reflect.TypeOf(component).Name()

	// add new entry into components map
	if _, exists := ecs.components[componentType]; !exists {
		// add component type to the maps
		ecs.componentIndex[componentType] = ecs.bitIndex
		ecs.components[componentType] = map[Entity]Component{}
		ecs.entityViews[componentType] = map[Entity]Bitfield{}

		// increment our bitIndex for the next component
		var lastIndex = ecs.bitIndex
		ecs.bitIndex *= 2
		if ecs.bitIndex < lastIndex {
			panic("Exceeded available flags for the bitfield! (max 32 b/c uint32)")
		}
	}
}

// AddComponent to a specific entity
func (ecs *ECS) AddComponent(entity Entity, component Component) {
	// get the component bit flag
	var componentType = reflect.TypeOf(component).Name()
	var componentFlag = ecs.componentIndex[componentType]

	ecs.entities[entity] = Set(ecs.entities[entity], componentFlag)
	ecs.components[componentType][entity] = component
	ecs.entityViews[componentType][entity] = ecs.entities[entity]
}

// GetComponent for a specific entity and component type
func (ecs *ECS) GetComponent(entity Entity, component Component) (Component, error) {
	// get the component bit flag
	var componentType = reflect.TypeOf(component).Name()
	var componentFlag = ecs.componentIndex[componentType]

	if Has(ecs.entities[entity], componentFlag) {
		return ecs.components[componentType][entity], nil
	}

	return nil, errors.New("No component for entity")
}

// EntitiesWith the provided set of components
func (ecs *ECS) EntitiesWith(comps ...Component) []Entity {
	// compute search flags and find smallest entity view
	var flags Bitfield
	var view string
	var viewSize = len(ecs.entities)
	for _, comp := range comps {
		// get the component bit flag
		var componentType = reflect.TypeOf(comp).Name()
		var componentFlag = ecs.componentIndex[componentType]

		flags = Set(flags, componentFlag)

		var numEntities = len(ecs.entityViews[componentType])
		if numEntities < viewSize {
			view = componentType
			viewSize = numEntities
		}
	}

	var entities = make([]Entity, 0)
	for uuid, bits := range ecs.entityViews[view] {
		if HasAll(bits, flags) {
			entities = append(entities, uuid)
		}
	}

	return entities
}
