# go-ecs

An entity component system built with Golang.

# Performance

I wanted to see what kind of performance I could achieve compared to the popular C++ libraries [EntityPlus](https://github.com/Yelnats321/EntityPlus) and [EntityX](https://github.com/alecthomas/entityx). EntityPlus has a benchmark in the baseline that will handle both it and EntityX, so I matched those tests and ran everything in my virtual machine and came out with the following numbers:

```
count   iterations  probability   entityplus  entityx   go-ecs  go-ecs views
1000    100000      1/3           3.7         14.8      5.4     3.9
10000   100000      1/3           36.7        157.8     62.6    54.0
30000   10000       1/3           11.9        49.1      17.8    14.5
100000  10000       1/5           26.2        106.3     41.6    30.2
10000   100000      1/1000        0.4         20.9      14.4    0.1
100000  1000000     1/1000        4.7         212.3     136.8   1.1
```

Before I put in a concept of per-component based views of the entities, go-ecs performance was somewhere between EntityPlus and EntityX, but definitely didn't keep up in the low probability scenarios. This was due to having to iterate over the entire entity list regardless of what component was being searched for. Once I included a secondary map that contains the list of entities with each component, the performance significantly increased in the low probability scenarios (to the point of being the fastest), and generally increased in the remaining cases. I suspect performance would increase further if I was keeping the entities in an array instead of a map, but I think at that point it's just to chase the record speeds.